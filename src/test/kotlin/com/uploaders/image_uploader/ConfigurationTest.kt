package com.uploaders.image_uploader

import com.uploaders.image_uploader.controllers.UploadController
import com.uploaders.image_uploader.services.ImageService
import com.uploaders.image_uploader.services.impl.ImageServiceImpl
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@TestConfiguration
@EnableWebMvc
class ConfigurationTest {

    @Bean
    fun uploadingController() = UploadController()

    @Bean
    fun imageService(): ImageService = ImageServiceImpl()

}