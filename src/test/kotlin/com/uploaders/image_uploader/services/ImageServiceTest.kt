package com.uploaders.image_uploader.services

import com.uploaders.image_uploader.ConfigurationTest
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration

@RunWith(SpringRunner::class)
@WebAppConfiguration
@ContextConfiguration(classes = [ConfigurationTest::class])
class ImageServiceTest {

    @Autowired
    lateinit var imageService: ImageService

    @Value(value = "classpath:meme1.jpg")
    lateinit var testMeme1: Resource

    @Value(value = "classpath:meme3.Base64.txt")
    lateinit var base64TestMeme3: Resource

    @Test
    fun saveEmptyByteArray() {
        assert(!imageService.save(byteArrayOf()))
    }

    @Test
    fun saveEmptyBase64String() {
        assert(!imageService.saveBase64(""))
    }

    @Test
    fun saveIncorrectByteArray() {
        assert(!imageService.save(byteArrayOf(0, 0, 0, 0, 1, 1)))
    }

    @Test
    fun saveIncorrectBase64String() {
        assert(!imageService.saveBase64("mdmdm==="))
    }

    @Test
    fun saveCorrectByteArray() {
        assert(imageService.save(testMeme1.file.readBytes()))
    }

    @Test
    fun saveCorrectBase64String() {
        assert(imageService.saveBase64(base64TestMeme3.file.readText()))
    }

}