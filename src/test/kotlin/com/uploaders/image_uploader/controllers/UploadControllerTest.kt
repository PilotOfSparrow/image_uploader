package com.uploaders.image_uploader.controllers

import com.uploaders.image_uploader.ConfigurationTest
import org.apache.http.entity.ContentType
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

@RunWith(SpringRunner::class)
@WebAppConfiguration
@ContextConfiguration(classes = [ConfigurationTest::class])
class UploadControllerTest {

    companion object {
        private const val MAPPING = "/upload"
    }

    @Autowired
    lateinit var webContext: WebApplicationContext

    @Value(value = "classpath:meme1.jpg")
    lateinit var testMeme1: Resource

    @Value(value = "classpath:meme2.jpg")
    lateinit var testMeme2: Resource

    @Value(value = "classpath:meme3.Base64.txt")
    lateinit var base64TestMeme3: Resource

    @Value(value = "classpath:meme4.Base64.txt")
    lateinit var base64TestMeme4: Resource

    private lateinit var mockMvc: MockMvc

    private val urlTestMeme5 =
        "https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fpuppy-meme.com%2Fwp-content%2Fuploads%2F2015%2F10%2F1445359345478.png"
    private val urlTestMeme6 = "https://priestlandscomputerscience.files.wordpress.com/2016/07/timthumb-php.jpg?w=554&h=338"

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext)
            .alwaysDo<DefaultMockMvcBuilder>(MockMvcResultHandlers.print())
            .build()
    }

    @Test
    fun uploadNothing() { // Positive because if we have nothing to save everything is saved
        mockMvc.perform(
            MockMvcRequestBuilders
                .post(MAPPING)
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadNullFile() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .file(createMockMultipartFile(UploadController.IMAGES_BY_FILE, null))
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.NEGATIVE_RESPONSE) }
    }

    @Test
    fun uploadSingleImageFile() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .file(createMockMultipartFile(UploadController.IMAGES_BY_FILE, testMeme1.file.readBytes()))
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadMultipleImageFiles() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .file(createMockMultipartFile(UploadController.IMAGES_BY_FILE, testMeme1.file.readBytes()))
                .file(createMockMultipartFile(UploadController.IMAGES_BY_FILE, testMeme2.file.readBytes()))
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadMixOfGoodAndEvilImageFiles() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .file(createMockMultipartFile(UploadController.IMAGES_BY_FILE, null))
                .file(createMockMultipartFile(UploadController.IMAGES_BY_FILE, testMeme2.file.readBytes()))
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.NEGATIVE_RESPONSE) }
    }

    @Test
    fun uploadNullBase64Image() { // Positive because params doesn't transform to emptyList, only files
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_BASE64, null)
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadWrongBase64Image() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_BASE64, "====")
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.NEGATIVE_RESPONSE) }
    }

    @Test
    fun uploadBase64Image() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_BASE64, base64TestMeme3.file.readText())
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadMultipleBase64Image() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_BASE64, base64TestMeme3.file.readText())
                .param(UploadController.IMAGES_BY_BASE64, base64TestMeme4.file.readText())
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadMixOfAngelAndDevilBase64Image() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_BASE64, null)
                .param(UploadController.IMAGES_BY_BASE64, String(base64TestMeme3.file.readBytes()))
                .param(UploadController.IMAGES_BY_BASE64, null)
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.NEGATIVE_RESPONSE) }
    }

    @Test
    fun uploadNullImageUrl() { // Positive because params doesn't transform to emptyList, only files
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_URL, null)
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadWrongImageUrl() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_URL, "https://memes-no-problemes")
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.NEGATIVE_RESPONSE) }
    }

    @Test
    fun uploadImageUrl() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_URL, urlTestMeme5)
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadMultipleImagesUrls() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_URL, urlTestMeme5)
                .param(UploadController.IMAGES_BY_URL, urlTestMeme6)
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.POSITIVE_RESPONSE) }
    }

    @Test
    fun uploadMixOfLoveAndHateImageUrls() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .multipart(MAPPING)
                .param(UploadController.IMAGES_BY_URL, null)
                .param(UploadController.IMAGES_BY_URL, urlTestMeme6)
                .param(UploadController.IMAGES_BY_URL, null)
                .param(UploadController.IMAGES_BY_URL, urlTestMeme5)
        ).andExpect { result -> assert(result.response.contentAsString == UploadController.NEGATIVE_RESPONSE) }
    }

    private fun createMockMultipartFile(
        name: String,
        bytes: ByteArray?,
        contentType: ContentType = ContentType.MULTIPART_FORM_DATA
    ) = MockMultipartFile(
        name,
        null,
        contentType.mimeType,
        bytes
    )

}