package com.uploaders.image_uploader.controllers

import com.uploaders.image_uploader.services.ImageService
import org.apache.commons.validator.routines.UrlValidator
import org.apache.http.impl.client.HttpClients
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.net.URI

@RestController
@RequestMapping("/upload")
class UploadController {

    companion object {
        const val POSITIVE_RESPONSE = "\nOk"
        const val NEGATIVE_RESPONSE = "\nInternal server error"

        const val IMAGES_BY_URL = "imageUrl"
        const val IMAGES_BY_FILE = "image"
        const val IMAGES_BY_BASE64 = "image64"
    }

    @Autowired
    lateinit var imageService: ImageService

    private val restTemplate = RestTemplateBuilder()
        .requestFactory { HttpComponentsClientHttpRequestFactory(HttpClients.createDefault()) }
        .build()

    @ResponseBody
    @PostMapping
    fun upload(
        @RequestParam(name = IMAGES_BY_URL, required = false) imagesUrls: List<String?>?,
        @RequestParam(name = IMAGES_BY_FILE, required = false) images: List<MultipartFile?>?,
        @RequestParam(name = IMAGES_BY_BASE64, required = false) images64: List<String?>?
    ): String = try {
        var isAllImagesSaved = images?.fold(true) { acc, imageMultipart ->
            imageMultipart?.bytes?.let(imageService::save) ?: false && acc
        } ?: true
        isAllImagesSaved = isAllImagesSaved && images64?.fold(isAllImagesSaved) { acc, imageBase64 ->
            imageBase64?.let(imageService::saveBase64) ?: false && acc
        } ?: true
        isAllImagesSaved = isAllImagesSaved && imagesUrls?.fold(isAllImagesSaved) { acc, url ->
            val imageBytes = url
                ?.takeIf(UrlValidator.getInstance()::isValid)
                ?.let { safeUrl -> restTemplate.getForObject(URI.create(safeUrl), ByteArray::class.java) }
                ?: ByteArray(0)
            imageService.save(imageBytes) && acc
        } ?: true
        if (isAllImagesSaved) POSITIVE_RESPONSE else NEGATIVE_RESPONSE // Further impr: send index of failed image
    } catch (e: Exception) { // Superior error handling
        NEGATIVE_RESPONSE
    }

}