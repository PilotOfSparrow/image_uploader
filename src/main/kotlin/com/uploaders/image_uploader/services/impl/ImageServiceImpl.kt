package com.uploaders.image_uploader.services.impl

import com.uploaders.image_uploader.services.ImageService
import org.springframework.stereotype.Service
import org.springframework.util.Base64Utils
import java.awt.Color
import java.awt.Image
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.File
import java.util.*
import javax.imageio.ImageIO

@Service
class ImageServiceImpl : ImageService {

    companion object {
        private const val PREVIEW_SIZE_PX = 100
    }

    private val savingFolder = System.getProperty("java.io.tmpdir") + "\\uploaded_images\\"
    private val previewSavingFolder = savingFolder + "preview\\"

    private val defaultScaledImageColor = Color(0, 0, 0)

    init {
        File(previewSavingFolder).mkdirs()
    }

    override fun save(bytes: ByteArray): Boolean = try {
        if (bytes.isEmpty()) throw IllegalStateException("Can't save image without bytes")
        ByteArrayInputStream(bytes).use { byteArrayInputStream ->
            ImageIO.createImageInputStream(byteArrayInputStream).use { imageInputStream ->
                val imageReader = ImageIO.getImageReaders(imageInputStream).next()!!.apply {
                    setInput(imageInputStream, true, true)
                }
                val image = imageReader.read(0, imageReader.defaultReadParam)
                val imageScaled = image.getScaledInstance(PREVIEW_SIZE_PX, PREVIEW_SIZE_PX, Image.SCALE_FAST)
                val imageName = "Image${UUID.randomUUID()}.${imageReader.formatName}"
                ImageIO.write(image, imageReader.formatName, File(savingFolder + imageName))
                ImageIO.write(
                    BufferedImage(PREVIEW_SIZE_PX, PREVIEW_SIZE_PX, image.type).apply {
                        graphics.drawImage(imageScaled, 0, 0, defaultScaledImageColor, null)
                    },
                    imageReader.formatName,
                    File(previewSavingFolder + "Preview.$imageName")
                )
            }
        }
        true
    } catch (e: Exception) {
        println(e.message)
        false
    }

    override fun saveBase64(imageInBase64: String): Boolean = try {
        save(Base64Utils.decodeFromString(imageInBase64))
    } catch (e: Exception) {
        false
    }

}