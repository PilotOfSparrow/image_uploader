package com.uploaders.image_uploader.services

interface ImageService {

    fun save(bytes: ByteArray): Boolean

    fun saveBase64(imageInBase64: String): Boolean

}