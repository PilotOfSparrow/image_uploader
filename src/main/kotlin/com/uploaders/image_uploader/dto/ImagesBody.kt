package com.uploaders.image_uploader.dto

data class ImagesBody(
    val images64: List<String>? = emptyList(),
    val imagesUrl: List<String>? = emptyList()
)