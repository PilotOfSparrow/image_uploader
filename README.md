# image_uploader

Севрер можно запустить через docker-compose up, также настроен pipeline для сборки.

Нагляднее всего демонстрация через запуск тестов в Intellij.

Загруженные картинки помещаются в /tmp/uploaded_images (или в ...AppData/Local/Temp/uploaded_images на Windows), там же превью в отдельной папке.
Если сервер сумел обработать все переданные изображения, то придёт "Ok", иначе "Internal server error"

Примеры с файлами:
curl -X POST --form "image=@{{ абсолютный путь до файла }}"  http://localhost:8000/upload // Тот же параметр для нескольких

Примеры с url:
curl -X POST --form "imageUrl={{ ссылка на изображение }} http://localhost:8000/upload // Тот же параметр для нескольких

Примеры с base64:
curl -X POST --form "image64={{ строка в base64 }} http://localhost:8000/upload // Тот же параметр для нескольких, base64 можно взять из src/test/resources