FROM gradle:5.0-jdk

USER root
RUN mkdir /build
WORKDIR /build

COPY . .
RUN gradle build

FROM openjdk:8-jre-alpine

RUN mkdir /app
WORKDIR /app

COPY --from=0 "build/build/libs/spring_uploader-1.0-SNAPSHOT.jar" /app

EXPOSE 8080

CMD java $JAVA_OPTS $JAVA_ARGS -jar "spring_uploader-1.0-SNAPSHOT.jar" --server.port=8080
